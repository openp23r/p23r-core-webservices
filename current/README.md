# Web Services for the P23R Processor

maintained by P23R-Team (a) Fraunhofer FOKUS

All web services required by the P23R processor are included here.
The web services are specified and explained in the related specification documents, especially the
`P23R: Spezifikationen zur Rahmenarchitektur`.