# Models for the P23R web services

All schemata and web service descriptions by the p23r components are included.

The data models and web services are specified and explained in the related
specification documents, especially the `P23R: Spezifikationen zur Rahmenarchitektur`.

The directory `current` contains all current data models and web service
descriptions while `previous` contains the outdated and deprecated versions.

---

This project is part of the *openP23R* initiative. All related projects
are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see *LICENSE* file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS
